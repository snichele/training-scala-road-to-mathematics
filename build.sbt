name := "training-scala-road-to-mathematics"

version := "1.0"

libraryDependencies ++= Seq(
    "ch.qos.logback" % "logback-classic" % "0.9.29",
    "org.slf4j" % "slf4j-api" % "1.6.4",
    "org.scalaz" %% "scalaz-core" % "6.0.4",
	"org.scalatest" %% "scalatest" % "2.2.1" % "test",
	"org.specs2" %% "specs2-core" % "2.4.15" % "test",
"org.scalacheck" %% "scalacheck" % "1.12.1" % "test"
)

testOptions in Test += Tests.Argument(TestFrameworks.ScalaCheck, "-maxSize", "5", "-minSuccessfulTests", "33", "-workers", "1", "-verbosity", "1")

scalacOptions += "-deprecation"