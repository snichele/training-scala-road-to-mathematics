package snichele.training.scala.roadtomathematics

import annotation.tailrec


object Main {
  object Chapter1_2 {
    /*
    A prime number is a natural number greater than 1 that has no proper divisors other than 1 and itself.
     The natural numbers are 0; 1; 2; 3; 4; : : :

     The list of prime numbers starts with 2; 3; 5; 7; 11; 13; : : :
     Except for 2, all of these are odd, of course.

     LD(n) for the least natural number greater than 1 that divides n
     A number d divides n if there is a natural numbera with a . d = n

     Proposition 1.2
      1. If n > 1 then LD(n) is a prime number.
      2. If n > 1 and n is not a prime number, then (LD(n))2 6 n.
     */

    /*
      divides :: Integer -> Integer -> Bool
      divides d n = rem n d == 0
     */
    def rem(n: Int, d: Int) = n % d

    /*
      divides :: (Int, Int) => Boolean
      divides.curried :: Int => (Int => Boolean)
    **/
    def divides(d: Int, n: Int) = (rem(n, d)) == 0

    /* ld n = ldf 2 n */
    def ld(n: Int) = ldf(2, n)

    /*
    ldf k n | divides k n = k
            | k^2 > n = n
            | otherwise = ldf (k+1) n
    */
    @tailrec
    def ldf(k: Int, n: Int): Int =
      if (divides(k, n))
        k
      else if ((k * k) > n)
        n
      else
        ldf(k + 1, n)

    /*
        prime0 n | n < 1 = error "not a positive integer"
        | n == 1 = False
        | otherwise = ld n == n

        Test if a number is prime (it's called composite otherwise)
    */
    def prime0(n: Int) =
      if (n < 1) sys.error("Not a positive integer")
      else if (n == 1) false
      else
        ld(n) == n

    /*
    mnmInt :: [Int] -> Int
    mnmInt [] = error "empty list"
    mnmInt [x] = x
    mnmInt (x:xs) = min x (mnmInt xs)

     min' :: Int -> Int -> Int
     min' x y | x <= y = x
             | otherwise = y
    */
    def minInt(x: Int, y: Int): Int = if (x <= y) x else y

    def maxInt(x: Int, y: Int): Int = if (x > y) x else y

    def minimumIntAmongst(xs: List[Int]): Int = {
      xs match {
        case Nil => sys.error("Empty list")
        case h :: Nil => h
        case h :: tail => minInt(h, minimumIntAmongst(tail))
      }
    }

    // Ex : 1.9
    def maximumIntAmongst(xs: List[Int]): Int = {
      xs match {
        case Nil => sys.error("Empty list")
        case h :: Nil => h
        case h :: tail => maxInt(h, maximumIntAmongst(tail))
      }
    }

    // Ex : 1.10
    /*
      Note the recursion is only applied if m is not removedd.
      Removing it stop it.
     */
    def removeFirstOccurenceAmongst(m: Int, xs: List[Int]): List[Int] = {
      xs match {
        case Nil => Nil
        case h :: Nil => if (h == m) Nil else xs
        case h :: tail => if (h == m) tail else h :: removeFirstOccurenceAmongst(m, tail)
      }
    }

    // Ex : 1.11
    /*
      srtInts :: [Int] -> [Int]
      srtInts [] = []
      srtInts xs = m : (srtInts (removeFst m xs)) where m = mnmInt xs

      srtInts' :: [Int] -> [Int]
      srtInts' [] = []
      srtInts' xs = let
                      m = mnmInt xs
                    in m : (srtInts' (removeFst m xs))
     */
    /*
      Warning : highly inefficient solution (list.length iterations over the tail !)
     */
    def sortInts(xs: List[Int]): List[Int] = {
      xs match {
        case Nil => Nil
        case _ => {
          val m = minimumIntAmongst(xs)
          m :: sortInts(removeFirstOccurenceAmongst(m, xs))
        }
      }
    }

    // Ex : 1.12
    /*
      average :: [Int] -> Float
      average [] = error "empty list"
      average xs = fromInt (sum xs) / fromInt (length xs)

      sum' :: [Int] -> Int
      sum' [] = 0
      sum' (x:xs) = x + sum' xs

      length' :: [a] -> Int
      length' [] = 0
      length' (x:xs) = 1 + length' xs

      Warning : again, this is higly inneficient...
    */
    def sum(xs: List[Int]): Int = {
      xs match {
        case Nil => 0
        case h :: t => h + sum(t)
      }
    }

    // Note the type scheme here (List[_]) : this functions works for any kinf of list !
    def length(xs: List[_]): Int = {
      xs match {
        case Nil => 0
        case _ :: t => 1 + length(t)
      }
    }

    def average(xs: List[Int]): Double = {
      xs match {
        case Nil => sys.error("Empty list !")
        case _ => sum(xs) / length(xs)
      }
    }

    // Ex : 1.13
    def countOccurencesOf(a: Char, s: String): Int = {
      countOccurencesOf(a,s.toList)
    }

    def countOccurencesOf(a: Char, s: List[Char]): Int = {
      s match {
        case Nil => 0
        case h :: Nil if (h.equals(a)) => 1
        case h :: t if (h.equals(a)) => 1 + countOccurencesOf(a, t)
        case h :: t => countOccurencesOf(a, t)
      }
    }
  }
}
