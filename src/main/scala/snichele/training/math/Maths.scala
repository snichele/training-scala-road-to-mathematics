package snichele.training.math

import scala.annotation.tailrec
import scala.util.Random

object Maths {

  val positiveOr0 = (_: Int) > 0

  val equal0 = (_: Int) == 0


  object DNAOperationsPuzzleGame {

    val operations = List(Ops.*, Ops.+, Ops./, Ops.-)

    def solve(numbers: List[Int]): Seq[(Int, Int, Operation)] = {

      def tryARandomSequenceOf(numbers: List[Int], ops: List[Operation]): Option[Computation] = {
        val ns = numbers//Random.shuffle(numbers)
        val os = ops //Random.shuffle(ops)

        @tailrec
        def doIt(previousComputation: Computation, numbers: List[Int], ops: List[Operation]): Option[Computation] = {
          previousComputation.value match {
            case Some(x) =>
              println("previousComputation value is " + x)
              ops match {
                case h :: Nil =>
                  println(s"-- Computation($x, $h, ${numbers.head}), ${numbers.tail}, ${ops.tail}")
                  Some(Computation(x, h, numbers.head))
                case h :: t =>
                  println(s"- Computation($x, $h, ${numbers.head}), ${numbers.tail}, ${ops.tail}")
                  doIt(Computation(x, h, numbers.head), numbers.tail, ops.tail)
                //case Nil => Some(previousComputation)
              }
            //case None => None
          }
        }
        val done = doIt(Computation(0, Ops.+, ns.head), ns.tail, os)
        println(s"Done is $done")
        done
      }

      // replace with a stream !
      var breaker = 0
      println(Stream.continually(tryARandomSequenceOf(numbers, operations)).takeWhile {
        comp =>
          breaker += 1
          if(comp.isDefined) println("***" + comp.get.value)
          (comp.isEmpty || comp.get.value.isEmpty || comp.get.value.get != 0) && breaker < 2
      }.filter(_.isDefined).toList.mkString("\n"))
      Seq()
    }
  }

  case class Computation(x: Int, op: Operation, y: Int) {
    def value: Option[Int] = op.f(x, y)
  }

  val fuckedUpComputation = Computation(1, Ops.+, 1)

  case class Operation(symbol: String, f: (Int, Int) => Option[Int])

  object Ops {
    val + = Operation("+", (x: Int, y: Int) => Some(x + y))
    val - = Operation("-", (x: Int, y: Int) => Some(x - y) filter positiveOr0)
    val * = Operation("*", (x: Int, y: Int) => Some(x * y))
    val / = Operation("/", (x: Int, y: Int) => Some(x % y) filter equal0 map { (_: Int) => x / y})
  }


}
