package snichele.training.projecteuler

object Euler {

  object Problem1 {
    def getListOfMatchingNumbers(below: Int): Seq[Int] = {

      def next(at: Int, in: Seq[Int]): Seq[Int] = {
        if (at == 0) {
          in
        } else {
          next(at - 1,
            if (at % 3 == 0 || at % 5 == 0) {
              in :+ at
            } else {
              in
            }
          )
        }
      }

      next(below - 1, Seq())
    }

    def compute(below: Int): Int = {
      0 to (below - 1) filter mod3Or5 reduceLeft (_ + _)
    }

    def mod3 = (x: Int) => x % 3 == 0

    def mod5 = (x: Int) => x % 5 == 0

    def mod3Or5 = (x: Int) => mod3(x) || mod5(x)
  }

  object Problem2 {

    def compute(): Int = {
      lazy val fibs: Stream[Int] = 0 #:: fibs.scanLeft(1)(_ + _)

      fibs filter (_ % 2 == 0) takeWhile (_ < 4000000) reduceLeft (_ + _)
    }

  }

  object Problem3 {


    /* Java version
    boolean isPrime(int n) {
      //check if n is a multiple of 2
      if (n%2==0) return false;
      //if not, then just check the odds
      for(int i=3;i*i<=n;i+=2) {
        if(n%i==0)
          return false;
      }
      return true;
    }
    */

    /*
    This 'naive' does not work...
    def isPrime(n: Int): Boolean = {
      if (n == 2) {
        true
      } else
      if (n % 2 == 0) {
        false
      } else {
        lazy val naturalsFrom3AndAdd2: Stream[Int] = Stream.cons(3, naturalsFrom3AndAdd2.map(_ + 2))
        naturalsFrom3AndAdd2 takeWhile (_ <= (n * n)) map (n % _ == 0) contains true
      }
    }*/

    object Prime {
      def is(i: Long) =
        if (i == 2) true
        else if ((i & 1) == 0) false // efficient div by 2
        else prime(i)

      def primes: Stream[Long] = 2 #:: prime3

      // performance: avoid redundant divide by two, so this starts at 3
      private val prime3: Stream[Long] = {
        def next(i: Long): Stream[Long] =
          if (prime(i))
            i #:: next(i + 2)
          else
            next(i + 2) // tail
        3 #:: next(5)
      }

      // assumes not even, check evenness before calling - perf note: must pass partially applied >= method
      private def prime(i: Long) =
        prime3 takeWhile math.sqrt(i).>= forall {
          i % _ != 0
        }
    }

    def isNotPrime = !Prime.is(_: Long)

    def findLargestDividingPrimeFactor(n: Long): Long = {
      if (Prime.is(n)) {
        n
      }else {
        lazy val numbersReverseFromN: Stream[Long] = Stream.cons(n, numbersReverseFromN.map(_ - 1))
        numbersReverseFromN find Prime.is head
      }
    }

  }

}
