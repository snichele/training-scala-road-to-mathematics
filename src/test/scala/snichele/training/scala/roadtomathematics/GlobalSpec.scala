package snichele.training.scala.roadtomathematics

import org.specs2.mutable._
import snichele.training.scala.roadtomathematics.Main.Chapter1_2._

class GlobalSpec extends Specification {


  "Scala adaptations of 'haskell road to mathematics' functions  " should {

    "  works for prime0 " in {
      // val somePrimes = Seq(2, 3, 5, 7, 11, 13, 17, 19, 23, 29)
      val numbersToTest = Seq(1, 2, 3, 4, 5, 6)
      numbersToTest map (prime0) must containAllOf(Seq(false, true, true, false, true, false))
    }

    "  works for minimum int from a list " in {
      val nums = List(8, 256, 2, 89, 1)
      minimumIntAmongst(nums) must be equalTo (1)
    }

    "  works for maximum int from a list " in {
      val nums = List(8, 256, 2, 89, 1)
      maximumIntAmongst(nums) must be equalTo (256)
    }
    "  works for removeFirstOccurenceAmongst " in {
      val nums = List(8, 256, 2, 89, 1, 2, 8)
      val result = removeFirstOccurenceAmongst(2, nums)
      (result must containAllOf(Seq(8, 256, 89, 1, 2, 8)).inOrder)
    }
    "  works for sortInts " in {
      val nums = List(8, 256, 2, 89, 1, 2, 8)
      val result = sortInts(nums)
      (result must containAllOf(Seq(1, 2, 2, 8, 8, 89, 256)).inOrder)
    }
    "  works for average " in {
      val nums = List(5,6,4,5,4,6)
      average(nums) must beEqualTo(5)
    }
    "  works for countOccurencesOf " in {
      val s = "This is a test"
      (countOccurencesOf('i',s) must beEqualTo(2)) and
      (countOccurencesOf('s',s) must beEqualTo(3))
    }
  }
}
