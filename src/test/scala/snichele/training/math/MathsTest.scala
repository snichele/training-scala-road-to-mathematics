package snichele.training.math

import org.specs2.mutable.Specification

class MathsTest extends Specification {

  "Maths tests " should {

    "  works for ... " in {
      Maths.DNAOperationsPuzzleGame.solve(List(13,15,35,10,23))
      1 must beEqualTo(1)
    }

  }
}
