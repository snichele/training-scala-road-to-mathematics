package snichele.training.projecteuler

import org.specs2.mutable._
import snichele.training.projecteuler.Euler._

class Main extends Specification {

  "Project euler puzzles  " should {

    "  works for ex1 " in {

      val result = Problem1.getListOfMatchingNumbers(10)
      (result must not beEmpty) and
        (result must containTheSameElementsAs(Seq(3, 5, 6, 9))) and
        (result.reduceLeft(_ + _) must beEqualTo(23))

      Problem1.compute(10) must beEqualTo(23)
    }

    "  works for ex2 " in {

      Problem2.compute() must beEqualTo(4613732)
    }

    "  works for ex3 " in {

      Problem3.Prime.is(2) must beTrue
      Problem3.Prime.is(3) must beTrue
      Problem3.Prime.is(5) must beTrue
      Problem3.Prime.is(10) must beFalse
      Problem3.Prime.is(11) must beTrue
      Problem3.Prime.is(18) must beFalse
      Problem3.Prime.is(23) must beTrue
      Problem3.Prime.is(807) must beFalse
      Problem3.Prime.is(4807) must beFalse
      Problem3.findLargestDividingPrimeFactor(4807) must beEqualTo(4801)
      Problem3.findLargestDividingPrimeFactor(600851475143L) must beEqualTo(600851475067L)

    }
  }
}
