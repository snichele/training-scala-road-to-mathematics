# The Scala Road to Logic, Math and Programming (and more )

This repository contains :

- a transcription in Scala of the code examples found in the excellent paper
 *The Haskell Road to Logic, Math and Programming*

Note that the orginial naming of the functions presented in the paper have been slightly altered for a sometimes
better semantic.

Original Haskell code (when provided) is included in the source (in comments) ; that's highly educationnal for
scala / haskell basic syntax comparison.

Functions are tested with Specs2.

- Mathematics coding experimentation

Let's re-learn mathematics (starting from lycée) by coding !